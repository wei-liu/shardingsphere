package org.apache.shardingsphere.core.parse;

import org.apache.commons.collections4.map.AbstractReferenceMap;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.shardingsphere.core.parse.cache.SafetyCache;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SQLParseEngineTest{
    private final static int READ_THREAD_NUM = 100;
    private final static int WRITE_THREAD_NUM = 10;
    private final static int ADD_COUNT = 1000000000;

    @Test
    @Ignore
    public void testReferenceMapConcurrency() throws InterruptedException {
        Map<Integer,Integer> map = new ReferenceMap<>(AbstractReferenceMap.ReferenceStrength.SOFT, AbstractReferenceMap.ReferenceStrength.SOFT, 1, 1);

        for(int i = 0;i < WRITE_THREAD_NUM;i++){
            Thread writeThread = new Thread(new Adder(i,map));
            writeThread.start();
        }

        TimeUnit.SECONDS.sleep(1);
        for(int i = 0;i < READ_THREAD_NUM;i++){
            Thread readThread = new Thread(new Reader(i,map));
            readThread.start();
        }

        TimeUnit.SECONDS.sleep(1000000);
    }

    @Test
    @Ignore
    public void testSafetyMapConcurrency() throws InterruptedException {
        Map<Integer,Integer> map = new SafetyCache<>();

        for(int i = 0;i < WRITE_THREAD_NUM;i++){
            Thread writeThread = new Thread(new Adder(i,map));
            writeThread.start();
        }

        TimeUnit.SECONDS.sleep(1);
        for(int i = 0;i < READ_THREAD_NUM;i++){
            Thread readThread = new Thread(new Reader(i,map));
            readThread.start();
        }

        TimeUnit.SECONDS.sleep(1000000);
    }

    static class Adder implements Runnable{
        private int threadNum;
        private Map<Integer,Integer> map;

        public Adder(int threadNum,Map<Integer, Integer> map) {
            this.threadNum = threadNum;
            this.map = map;
        }

        @Override
        public void run() {
            while (true){
                for(int i = 0;i < ADD_COUNT;i++){
                    map.put(i,0);
                    System.out.println("thread num: " + threadNum + ",put: " + i);
                }
            }
        }
    }

    static class Reader implements Runnable{
        private int threadNum;
        private Map<Integer,Integer> map;

        public Reader(int threadNum, Map<Integer, Integer> map) {
            this.threadNum = threadNum;
            this.map = map;
        }

        @Override
        public void run() {
            while (true){
                for(int i = 0;i < ADD_COUNT;i++){
                    System.out.println("thread num: " + threadNum + "," +map.get(i));
                }
            }
        }
    }
}


