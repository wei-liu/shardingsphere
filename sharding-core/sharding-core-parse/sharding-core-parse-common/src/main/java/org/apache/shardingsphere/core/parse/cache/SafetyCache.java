package org.apache.shardingsphere.core.parse.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class SafetyCache<K, V> implements Map<K, V> {

    private static Logger logger = LoggerFactory.getLogger(SafetyCache.class);

    private final Cache<K, V> cache = CacheBuilder.newBuilder().softValues().initialCapacity(2000).maximumSize(65535).build();

    public SafetyCache() {
        logger.info("init SafetyCache!");
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        logger.debug("SafetyCache get by key: {}",key);
        return cache.getIfPresent((K)key);
    }

    @Override
    public V put(K key, V value) {
        logger.debug("SafetyCache put by key: {},value: {}",key,value);
        cache.put(key,value);
        return value;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}
